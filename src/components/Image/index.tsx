import React from 'react'
import { test1 } from '../../../public/'

type Props = {
  alt: string
}

const Image: React.FC<Props> = ({ alt }) => {
  return (
    <div>
      <img src={test1} alt={alt} />
    </div>
  )
}

export default Image
